#ifndef STROMPI_OPTIONS_HPP
#define STROMPI_OPTIONS_HPP
#include <chrono>
#include <optional>
#include <string>

namespace strompi::options {
struct config {
  std::string serial{"/dev/serial0"};
  int status_read_interval_seconds{1};
};
std::optional<config> parse(int argc, char *argv[]);

} // namespace strompi::options
#endif
