.PHONY: all configure build dependencies clean

all: build

build: configure
	meson compile -C builddir

configure: build/build.ninja

build/build.ninja: dependencies
	meson setup --pkg-config-path dependencies/ builddir .

dependencies: dependencies/conaninfo.txt

dependencies/conaninfo.txt:
	conan install --build missing --install-folder dependencies .

clean:
	rm -rf build dependencies

