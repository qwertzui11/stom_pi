#include "options.hpp"
#include "strompi/events/parser.hpp"
#include "strompi/reader.hpp"
#include "strompi/serial/device.hpp"
#include "strompi/serial/line_reader.hpp"
#include "strompi/serial/line_writer.hpp"
#include "strompi/serial/opener.hpp"
#include "strompi/status/parser.hpp"
#include "strompi/status/trigger.hpp"
#include <boost/asio/io_context.hpp>
#include <boost/asio/signal_set.hpp>
#include <iostream>

using namespace strompi;

int main(int argc, char *argv[]) {
  const auto maybe_config = options::parse(argc, argv);
  if (!maybe_config)
    return 0;
  const options::config config = maybe_config.value();

  std::cout << "opening device:" << config.serial << std::endl;

  boost::asio::io_context context;
  serial::opener opener{context.get_executor(), config.serial};
  serial::device device = opener();
  status::parser status_parser;
  strompi::serial::line_writer line_writer(context, device);
  strompi::status::trigger status_trigger(line_writer);
  strompi::serial::line_reader line_reader{device};
  strompi::events::parser events_parser;
  std::chrono::steady_clock::duration read_interval =
      std::chrono::seconds(config.status_read_interval_seconds);
  strompi::reader reader{context,       line_reader,   status_trigger,
                         status_parser, events_parser, read_interval};
  reader.on_status = [](auto status_) {
    std::cout << "status:" << status_ << std::endl;
  };
  reader.on_event = [](auto event_) {
    std::cout << "event:" << event_ << std::endl;
  };

  boost::asio::signal_set signals(context, SIGINT, SIGTERM);
  boost::asio::spawn([&](boost::asio::yield_context yield) {
    const int signal = signals.async_wait(yield);
    std::cout << "received signal '" << signal << "'" << std::endl;
    reader.stop();
    device.serial_port.close();
  });

  context.run();

  return 0;
}
