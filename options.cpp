#include "options.hpp"
#include <boost/program_options.hpp>
#include <iostream>

using namespace strompi;
namespace bpo = boost::program_options;

std::optional<options::config> options::parse(int argc, char **argv) {

  options::config result;
  bpo::options_description description;
  description.add_options()("help", "produce help description");
  description.add_options()(
      "device",
      bpo::value<std::string>(&result.serial)->default_value(result.serial),
      "the serial device to use");
  description.add_options()(
      "status-read-interval",
      bpo::value<int>(&result.status_read_interval_seconds)
          ->default_value(result.status_read_interval_seconds),
      "interval of reading status in seconds");
  bpo::variables_map variables_map;
  auto parsed_command_line = bpo::parse_command_line(argc, argv, description);
  bpo::store(parsed_command_line, variables_map);
  bpo::notify(variables_map);
  if (variables_map.count("help") >= 1) {
    std::cout << description << std::endl;
    return {};
  }
  return result;
}
