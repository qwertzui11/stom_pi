#ifndef STROMPI_READER_HPP
#define STROMPI_READER_HPP

#include "events/type.hpp"
#include "status/entity.hpp"
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>
#include <chrono>
#include <functional>

namespace strompi {
namespace status {
class parser;
class trigger;
} // namespace status
namespace events {
class parser;
}
namespace serial {
class line_reader;
}
class reader {
public:
  reader(boost::asio::io_context &io_context, serial::line_reader &line_reader,
         status::trigger &status_trigger, status::parser &status_parser,
         events::parser &events_parser,
         const std::chrono::steady_clock::duration &read_interval);

  void stop();
  std::function<void(events::type)> on_event;
  std::function<void(status::entity)> on_status;

private:
  void trigger(boost::asio::yield_context yield,
               const std::chrono::steady_clock::duration &trigger_interval);
  void read(boost::asio::yield_context yield);

  serial::line_reader &line_reader;
  status::trigger &status_trigger;
  status::parser &status_parser;
  events::parser &events_parser;
  boost::asio::steady_timer timer;
};
} // namespace strompi

#endif

