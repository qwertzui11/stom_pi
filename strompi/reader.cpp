#include "reader.hpp"
#include "strompi/events/parser.hpp"
#include "strompi/serial/line_reader.hpp"
#include "strompi/status/parser.hpp"
#include "strompi/status/trigger.hpp"
#include <iostream>

using namespace strompi;

reader::reader(boost::asio::io_context &io_context,
               serial::line_reader &line_reader,
               status::trigger &status_trigger, status::parser &status_parser,
               events::parser &events_parser,
               const std::chrono::steady_clock::duration &read_interval)
    : line_reader(line_reader), status_trigger(status_trigger),
      status_parser(status_parser), events_parser(events_parser),
      timer(io_context) {
  boost::asio::spawn([this](boost::asio::yield_context yield) { read(yield); });
  boost::asio::spawn([this, read_interval](boost::asio::yield_context yield) {
    trigger(yield, read_interval);
  });
}

void reader::stop() { timer.cancel(); }

void reader::trigger(
    boost::asio::yield_context yield,
    const std::chrono::steady_clock::duration &trigger_interval) {
  try {
    while (true) {
      status_parser.reset();
      status_trigger(yield);
      timer.expires_from_now(trigger_interval);
      timer.async_wait(yield);
    }
  } catch (const boost::system::system_error &error) {
    if (error.code() == boost::asio::error::operation_aborted)
      return;
    std::cerr << "goiing to throw the error: " << error.what() << std::endl;
    throw error;
  }
}

void reader::read(boost::asio::yield_context yield) {
  try {
    while (true) {
      const auto line = line_reader(yield);
      const auto event = events_parser.parse_line(line);
      if (event) {
        if (!on_event)
          continue;
        on_event(event.value());
        continue;
      }
      const auto status_ = status_parser.handle_line(line);
      if (!status_)
        continue;
      if (!on_status)
        continue;
      on_status(status_.value());
    }
  } catch (const boost::system::system_error &error) {
    if (error.code() == boost::asio::error::operation_aborted)
      return;
    std::cerr << "goiing to throw the error: " << error.what() << std::endl;
    throw error;
  }
}
