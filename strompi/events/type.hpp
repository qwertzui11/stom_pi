#ifndef STROMPI_EVENTS_TYPE_HPP
#define STROMPI_EVENTS_TYPE_HPP
#include <ostream>

namespace strompi::events {

enum class type { shutdown, power_fail, power_back };
std::ostream &operator<<(std::ostream &out, const type to_print);

} // namespace strompi::events

#endif

