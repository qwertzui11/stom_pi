#ifndef STROMPI_EVENTS_PARSER_HPP
#define STROMPI_EVENTS_PARSER_HPP
#include "type.hpp"
#include <optional>
#include <string_view>

namespace strompi::events {

class parser {
public:
  std::optional<type> parse_line(std::string_view line);
};

} // namespace strompi::events

#endif
