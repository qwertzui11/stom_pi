#include "type.hpp"

std::ostream &strompi::events::operator<<(std::ostream &out,
                                          const type to_print) {
  switch (to_print) {
  case type::power_back:
    return out << "power_back";
  case type::power_fail:
    return out << "power_fail";
  case type::shutdown:
    return out << "shutdown";
  }
  throw std::runtime_error("invalid enum value");
}

