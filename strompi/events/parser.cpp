#include "parser.hpp"

using namespace strompi::events;

std::optional<type> parser::parse_line(std::string_view line) {
  if (line == "xxxShutdownRaspberryPixxx")
    return type::shutdown;
  if (line == "xxx--StromPiPowerfail--xxx")
    return type::power_fail;
  if (line == "xxx--StromPiPowerBack--xxx")
    return type::power_back;
  return {};
}

