#ifndef STROMPI_STATUS_TIGGER_HPP
#define STROMPI_STATUS_TIGGER_HPP
#include <boost/asio/spawn.hpp>

namespace strompi {
namespace serial {
class line_writer;
}
namespace status {
class trigger {
public:
  trigger(serial::line_writer &writer);
  void operator()(boost::asio::yield_context yield);

private:
  serial::line_writer &writer;
};
} // namespace status
} // namespace strompi

#endif
