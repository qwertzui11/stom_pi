#include "entity.hpp"

std::ostream &strompi::status::operator<<(std::ostream &out,
                                          const entity &to_print) {
  out << "{voltages:" << to_print.voltages_ << ", battery:" << to_print.battery_
      << ", source:";
  if (to_print.source_)
    out << to_print.source_.value();
  else
    out << "--";
  return out << "}";
}

std::ostream &strompi::status::operator<<(std::ostream &out,
                                          const entity::battery &to_print) {
  out << "{charging:" << to_print.charging << ", level:";
  if (to_print.level_)
    out << to_print.level_.value();
  else
    out << "--";
  return out << "}";
}

std::ostream &strompi::status::
operator<<(std::ostream &out, const entity::battery::level &to_print) {
  switch (to_print) {
  case entity::battery::level::percent10:
    return out << "10%";
  case entity::battery::level::percent25:
    return out << "25%";
  case entity::battery::level::percent50:
    return out << "50%";
  case entity::battery::level::percent100:
    return out << "100%";
  }
  throw std::runtime_error("invalid battery::level");
}

std::ostream &strompi::status::operator<<(std::ostream &out,
                                          const entity::voltages &to_print) {
  return out << "{battery:" << to_print.battery << "V, wide:" << to_print.wide
             << "V, usb:" << to_print.usb << "V, output:" << to_print.output
             << "V}";
}

std::ostream &strompi::status::operator<<(std::ostream &out,
                                          const entity::source &to_print) {
  switch (to_print) {
  case entity::source::battery:
    return out << "battery";
  case entity::source::usb:
    return out << "usb";
  case entity::source::wide:
    return out << "wide";
  }
  throw std::runtime_error("invalid source");
}
