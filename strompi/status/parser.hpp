#ifndef STROMPI_STATUS_PARSER_HPP
#define STROMPI_STATUS_PARSER_HPP
#include "entity.hpp"
#include <optional>

namespace strompi::status {
class parser {
public:
  void reset();
  std::optional<entity> handle_line(std::string_view line);

private:
  int counter{};
  entity result;
};
} // namespace strompi::status

#endif
