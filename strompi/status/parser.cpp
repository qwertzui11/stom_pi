#include "parser.hpp"
#include <string>
#include <string_view>

using namespace strompi::status;

void parser::reset() {
  counter = 0;
  result = entity{};
}

static int parse_int(std::string_view line) {
  const std::string line_casted(line);
  return std::stoi(line_casted);
}

static float parse_volt(std::string_view line) {
  const int value = parse_int(line);
  return static_cast<float>(value) / 1000.f;
}

static bool parse_bool(std::string_view line) {
  const int value = parse_int(line);
  return value == 1;
}

static std::optional<entity::battery::level>
parse_battery_level(std::string_view line) {
  int battery_level = parse_int(line);
  switch (battery_level) {
  case 1:
    return entity::battery::level::percent10;
  case 2:
    return entity::battery::level::percent25;
  case 3:
    return entity::battery::level::percent50;
  case 4:
    return entity::battery::level::percent100;
  }
  return {};
}

static std::optional<entity::source> parse_source(std::string_view line) {
  int source_ = parse_int(line);
  switch (source_) {
  case 1:
    return entity::source::usb;
  case 2:
    return entity::source::wide;
  case 3:
    return entity::source::battery;
  }
  return {};
}

std::optional<entity> parser::handle_line(std::string_view line) {
  ++counter;
  if (counter == 23) {
    result.battery_.level_ = parse_battery_level(line);
    return {};
  }
  if (counter == 24) {
    result.battery_.charging = parse_bool(line);
    return {};
  }
  if (counter == 32) {
    result.voltages_.wide = parse_volt(line);
    return {};
  }
  if (counter == 33) {
    result.voltages_.battery = parse_volt(line);
    return {};
  }
  if (counter == 34) {
    result.voltages_.usb = parse_volt(line);
    return {};
  }
  if (counter == 35) {
    result.voltages_.output = parse_volt(line);
    return {};
  }
  if (counter == 36) {
    result.source_ = parse_source(line);
    return {};
  }
  if (counter == 38) {
    auto return_value = result;
    reset();
    return return_value;
  }
  return {};
}

