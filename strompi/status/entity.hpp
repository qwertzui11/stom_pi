#ifndef STROMPI_STATUS_ENTITY_HPP
#define STROMPI_STATUS_ENTITY_HPP

#include <optional>
#include <ostream>

namespace strompi::status {
struct entity {
  struct voltages {
    float wide{};
    float battery{};
    float usb{};
    float output{};
  };
  voltages voltages_{};
  struct battery {
    enum class level { percent10, percent25, percent50, percent100 };
    std::optional<level> level_;
    bool charging{};
  };
  battery battery_{};
  enum class source { usb, battery, wide };
  std::optional<source> source_;
};

std::ostream &operator<<(std::ostream &out, const entity &to_print);
std::ostream &operator<<(std::ostream &out, const entity::voltages &to_print);
std::ostream &operator<<(std::ostream &out, const entity::battery &to_print);
std::ostream &operator<<(std::ostream &out,
                         const entity::battery::level &to_print);
std::ostream &operator<<(std::ostream &out, const entity::source &to_print);

} // namespace strompi::status
#endif
