#include "trigger.hpp"
#include "strompi/serial/line_writer.hpp"
#include <iostream>

using namespace strompi::status;

trigger::trigger(serial::line_writer &writer) : writer(writer) {}

void trigger::operator()(boost::asio::yield_context yield) {
  writer(yield, "status-rpi");
}
