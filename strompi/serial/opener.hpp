#ifndef STROMPI_SERIAL_OPENER_HPP
#define STROMPI_SERIAL_OPENER_HPP
#include "device.hpp"
#include <boost/asio/spawn.hpp>

namespace strompi::serial {
class opener {
public:
  opener(boost::asio::executor executor, const std::string_view path);
  device operator()();

private:
  boost::asio::executor executor;
  std::string path;
};
} // namespace strompi::serial

#endif
