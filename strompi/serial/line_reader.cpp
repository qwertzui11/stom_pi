#include "line_reader.hpp"
#include <boost/asio/read_until.hpp>

using namespace strompi::serial;

line_reader::line_reader(serial::device &device) : device(device) {}

std::string line_reader::operator()(boost::asio::yield_context yield) {
  auto &serial_port = device.serial_port;
  static constexpr std::string_view new_line = "\n";
  const auto read_count = boost::asio::async_read_until(
      serial_port, boost::asio::dynamic_buffer(buffer), new_line, yield);
  const auto read_count_without_new_line =
      read_count - static_cast<int>(new_line.size());
  const auto result = buffer.substr(0, read_count_without_new_line);
  buffer.erase(0, read_count);
  return result;
}
