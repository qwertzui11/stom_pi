#ifndef STROMPI_SERIAL_LINE_WRITER_HPP
#define STROMPI_SERIAL_LINE_WRITER_HPP
#include <boost/asio/spawn.hpp>
#include <boost/asio/steady_timer.hpp>

namespace strompi::serial {
struct device;

class line_writer {
public:
  line_writer(boost::asio::io_context &context, serial::device &device);
  void operator()(boost::asio::yield_context yield,
                  const std::string_view line);

private:
  void wait_for_some_time(boost::asio::yield_context yield);

  boost::asio::steady_timer timer;
  device &device_;
};
} // namespace strompi::serial
#endif
