#ifndef STROMPI_SERIAL_LINE_READER_HPP
#define STROMPI_SERIAL_LINE_READER_HPP

#include "device.hpp"
#include <boost/asio/spawn.hpp>

namespace strompi::serial {
class line_reader {
public:
  line_reader(serial::device &device);
  std::string operator()(boost::asio::yield_context yield);

private:
  serial::device &device;
  std::string buffer;
};
} // namespace strompi::serial

#endif
