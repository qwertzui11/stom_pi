#ifndef STROMPI_SERIAL_DEVICE_HPP
#define STROMPI_SERIAL_DEVICE_HPP
#include <boost/asio/serial_port.hpp>

namespace strompi::serial {
struct device {
  boost::asio::serial_port serial_port;
};
} // namespace strompi::serial

#endif
