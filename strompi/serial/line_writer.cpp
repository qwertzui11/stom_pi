#include "line_writer.hpp"
#include "strompi/serial/device.hpp"
#include <boost/asio/write.hpp>

using namespace strompi::serial;

line_writer::line_writer(boost::asio::io_context &context,
                         serial::device &device_)
    : timer(context), device_(device_) {}

void line_writer::operator()(boost::asio::yield_context yield,
                             const std::string_view line) {
  const std::size_t interval{100};
  for (std::size_t index{}; index < line.size(); index += interval) {
    const auto to_send = line.substr(index, interval);
    [[maybe_unused]] const auto bytes_written = boost::asio::async_write(
        device_.serial_port, boost::asio::buffer(to_send), yield);
    wait_for_some_time(yield);
  }
  static const std::string commit = "\r";
  boost::asio::async_write(device_.serial_port, boost::asio::buffer(commit),
                           yield);
  wait_for_some_time(yield);
}

void line_writer::wait_for_some_time(boost::asio::yield_context yield) {
  timer.expires_from_now(std::chrono::milliseconds(100));
  timer.async_wait(yield);
}
