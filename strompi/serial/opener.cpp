#include "opener.hpp"

using namespace strompi::serial;

opener::opener(boost::asio::executor executor, const std::string_view path)
    : executor{executor}, path{path} {}

static void set_options(boost::asio::serial_port &handle) {
  /*
   * from the original "StromPi3_Status.py" python3 source
  serial_port.baudrate = 38400
  serial_port.port = '/dev/serial0'
  serial_port.timeout = 1
  serial_port.bytesize = 8
  serial_port.stopbits = 1
  serial_port.parity = serial.PARITY_NONE
   */

  handle.set_option(boost::asio::serial_port_base::baud_rate(38400));

  handle.set_option(boost::asio::serial_port_base::character_size(8));
  handle.set_option(boost::asio::serial_port_base::stop_bits(
      boost::asio::serial_port_base::stop_bits::one));
  handle.set_option(boost::asio::serial_port_base::parity(
      boost::asio::serial_port_base::parity::none));
  // handle.set_option(boost::asio::serial_port_base::flow_control(
  //    boost::asio::serial_port_base::flow_control::none));
}

device opener::operator()() {
  boost::asio::serial_port native{executor};
  native.open(path);
  set_options(native);
  device result{std::move(native)};
  return result;
}
